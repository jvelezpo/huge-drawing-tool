package com.sebastian.hugetest.presentation;

import java.util.ArrayList;
import java.util.List;

import com.sebastian.hugetest.business.CanvasProcessor;
import com.sebastian.hugetest.drawing.Canvas;

/**
 * Drawing tool
 * Createa a new canvas, Start drawing on it or quit the application
 * 
 * @author Sebastian
 *
 */
public class Nutshell {

	private List<Canvas> canvasList;
	private InputAsker asker;
	private CanvasProcessor processor;

	public Nutshell(InputAsker asker) {
		
		this.asker = asker;
		
		this.canvasList = new ArrayList<Canvas>();
		this.processor = new CanvasProcessor(asker);
	}

	/**
	 * Main menu of the app, shows the 3 basic options 
	 */
	public void inflatePrincipalMenu() {
		while (true) {

			if (this.canvasList.size() > 0) {
				System.out.println("1. Create a new canvas, (You already have one canvas created, pick option 2 nad lets paint over it)");
			} else {
				System.out.println("1. Create a new canvas");
			}
			System.out.println("2. Start drawing on the canvas by issuing various commands");
			System.out.println("3. Quit");

			char input = '0';
			try {
				input = asker.getInput().charAt(0);
			} catch (Exception e) {
				System.err.println("Enter, Really?");
			}

			handleInput(input);
		}
	}

	/**
	 * Handles input on the main menu
	 * 
	 * @param input Keyboard input in main menu
	 */
	private void handleInput(Character input) {
		
		switch (input) {
		case '1':
			Canvas canvas = createNewCanvas();
			
			// This is only for now
			this.canvasList.clear();

			if (canvas != null) {
				this.canvasList.add(canvas);
			}
			
			break;
		case '2':
			if (this.canvasList.size() > 0) {
				CanvasNutshell canvasNutshell = new CanvasNutshell(this.canvasList.get(0), this.asker);
				canvasNutshell.inflatePrincipalMenu();
			} else {
				System.err.println("There is no canvas yet created, please select option 1 and create on first");
			}
			break;
		case '3':
			System.out.println("Bye Bye!");
			System.exit(0);
		default:
			System.err.println("Invalid Option!");
			break;
		}
	}

	/**
	 * Creates the canvas where the user is going to draw beautiful art 
	 * 
	 * @return Instance of Canvas
	 */
	private Canvas createNewCanvas() {
		
		return this.processor.createNewCanvas();
	}
}
