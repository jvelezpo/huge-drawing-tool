package com.sebastian.hugetest.presentation;

import com.sebastian.hugetest.business.CanvasProcessor;
import com.sebastian.hugetest.drawing.Canvas;
import com.sebastian.hugetest.drawing.CanvasDrawer;
import com.sebastian.hugetest.exception.InvalidInputException;

public class CanvasNutshell {

	private Canvas canvas;
	private InputAsker asker;
	private CanvasDrawer drawer;
	private CanvasProcessor processor;

	public CanvasNutshell(Canvas canvas, InputAsker asker) {
		
		this.canvas = canvas;
		this.asker = asker;
		
		this.drawer = new CanvasDrawer(canvas);
		this.processor = new CanvasProcessor(asker);
		this.processor.setDrawer(drawer);
	}

	/* Gettes && Setters */

	public Canvas getCanvas() {
		return canvas;
	}

	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}

	public InputAsker getAsker() {
		return asker;
	}

	public void setAsker(InputAsker asker) {
		this.asker = asker;
	}

	public CanvasDrawer getDrawer() {
		return drawer;
	}

	public void setDrawer(CanvasDrawer drawer) {
		this.drawer = drawer;
	}
	
	public CanvasProcessor getProcessor() {
		return processor;
	}

	public void setProcessor(CanvasProcessor processor) {
		this.processor = processor;
	}

	/**
	 * Menu of the app, shows optios to draw on the canvas that was already created 
	 */
	public void inflatePrincipalMenu() {
		
		boolean exit = false;
		
		this.drawer.drawIt();
		
		while (!exit) {
			System.out.print("enter command: ");

			String input = null;
			try {
				input = asker.getInput();
			} catch (Exception e) {
				System.err.println("Enter, Really?");
			}

			exit = handleInput(input);
		}
	}

	/**
	 * Handles input on the menu, and performs the correct action
	 * 
	 * @param input Keyboard input in main menu
	 */
	private boolean handleInput(String input) {

		String[] tokens = input.split("\\s+");
		boolean goodInput = false;
		boolean result = false;

		try {
			switch (tokens[0]) {
			case "L":
			case "l":
				goodInput = validateInput(tokens, 5);

				if (!goodInput) {
					throw new InvalidInputException();
				}
				this.processor.doALine(this.canvas, tokens);
				break;

			case "R":
			case "r":
				goodInput = validateInput(tokens, 5);

				if (!goodInput) {
					throw new InvalidInputException();
				}

				this.processor.doARectangle(this.canvas, tokens);
				break;
			case "B":
			case "b":
				goodInput = validateInput(tokens, 4);

				if (!goodInput) {
					throw new InvalidInputException();
				}

				this.processor.fillArea(canvas, tokens);
				break;
			case "Q":
			case "q":
				System.out.println("Going back!");
				result = true;
				break;
			default:
				System.err.println("Invalid Option!");
				break;
			}
		} catch (InvalidInputException iie) {
			System.err.println(iie.getMessage());
		}
		return result;
	}

	/**
	 * Validates a correct keyboard input on the drawer menu
	 * 
	 * @param tokens
	 * @param numberOfTokens
	 * @return true or false, base on the input if it is correct or not
	 */
	private boolean validateInput(String[] tokens, Integer numberOfTokens) {

		return processor.validateDrawMenuInput(tokens, numberOfTokens);
	}
}
