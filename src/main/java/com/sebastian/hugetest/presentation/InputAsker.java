package com.sebastian.hugetest.presentation;

import java.io.InputStream;
import java.util.Scanner;

/**
 * Wrapper for system input functions.
 * This is made so testing task will be simpler, and cleaner code
 * 
 * @author Sebastian
 *
 */
public class InputAsker {

	private final Scanner scanner;
    
    public InputAsker(InputStream in) {
    	scanner = new Scanner(in);
    }
    
    public String getInput() {
    	return scanner.nextLine().trim();
    }
}
