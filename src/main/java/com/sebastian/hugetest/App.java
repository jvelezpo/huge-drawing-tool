package com.sebastian.hugetest;

import com.sebastian.hugetest.presentation.InputAsker;
import com.sebastian.hugetest.presentation.Nutshell;

/**
 * Drawing tool You're given the task of writing a simple console version of a
 * drawing program. At this time, the functionality of the program is quite
 * limited but this might change in the future. In a nutshell, the program
 * should work as follows: 
 * 1. Create a new canvas 
 * 2. Start drawing on the canvas by issuing various commands 
 * 3. Quit
 * 
 * @author Sebastian
 *
 */
public class App {

	public static void main(String[] args) {

		Nutshell nutshell = new Nutshell(new InputAsker(System.in));

		nutshell.inflatePrincipalMenu();
	}
}
