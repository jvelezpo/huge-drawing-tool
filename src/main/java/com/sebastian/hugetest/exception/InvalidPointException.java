package com.sebastian.hugetest.exception;

/**
 * Shows and appropriate exception when the point entered is invalid.
 * @author Sebastian
 *
 */
public class InvalidPointException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public InvalidPointException(){
		super("Sorry the points you enter are not correct.");
	}
}
