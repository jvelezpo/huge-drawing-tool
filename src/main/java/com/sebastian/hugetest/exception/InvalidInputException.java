package com.sebastian.hugetest.exception;

/**
 * Shows and appropriate exception when the keyboard input is not correct
 * @author Sebastian
 *
 */
public class InvalidInputException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public InvalidInputException(){
		super("Invalid Input!");
	}
}
