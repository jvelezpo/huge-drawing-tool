package com.sebastian.hugetest.business;

import com.sebastian.hugetest.drawing.Canvas;
import com.sebastian.hugetest.drawing.CanvasDrawer;
import com.sebastian.hugetest.drawing.Point;
import com.sebastian.hugetest.exception.InvalidInputException;
import com.sebastian.hugetest.exception.InvalidPointException;
import com.sebastian.hugetest.presentation.InputAsker;

public class CanvasProcessor {

	private InputAsker asker;
	private CanvasDrawer drawer;
	
	public CanvasProcessor(InputAsker asker) {
		this.asker = asker;
	}
	
	/* Gettes && Setters */
	
	public InputAsker getAsker() {
		return asker;
	}

	public void setAsker(InputAsker asker) {
		this.asker = asker;
	}

	public CanvasDrawer getDrawer() {
		return drawer;
	}

	public void setDrawer(CanvasDrawer drawer) {
		this.drawer = drawer;
	}

	/**
	 * Creates the canvas where the user is going to draw beautiful art 
	 * 
	 * @return Instance of Canvas
	 */
	public Canvas createNewCanvas() {
		System.out.println("Please enter the width and height separate by an space");

		Canvas canvas = null;

		try {
			String input = asker.getInput();

			String[] tokens = input.split("\\s+");

			if (tokens.length != 2) {
				throw new InvalidInputException();
			}

			int width = Integer.parseInt(tokens[0]);
			int height = Integer.parseInt(tokens[1]);
			
			if (width < 1 || height < 1) {
				throw new InvalidInputException();
			}

			canvas = new Canvas(width, height);
			
			// Set the new canvas on the drawer
			if (this.drawer == null) {

				this.drawer = new CanvasDrawer(canvas);
			} else {

				this.drawer.setCanvas(canvas);
			}
			
			this.drawer.doTheEdgesOnTheCanvas();
			this.drawer.drawIt();

			System.out.println("Canvas was created successfully, now you ca do some drawing");

		} catch (InvalidInputException | NumberFormatException e) {

			System.err.println(new InvalidInputException().getMessage());
		}
		return canvas;
	}
	
	/**
	 * L x1 y1 x2 y2 Should create a new line from (x1,y1) to (x2,y2). Currently
	 * only horizontal or vertical lines are supported. Horizontal and vertical
	 * lines will be drawn using the 'x' character.
	 * 
	 * @param canvas
	 * @param tokens
	 */
	public void doALine(Canvas canvas, String[] tokens) {

		try {
			Point[] points = getPoints(canvas, tokens, 2);

			boolean isThereALine = validateThereIsVerticalOrHorizontalLine(
					points[0], points[1]);

			if (isThereALine) {

				drawer.doALineOnTheCanvas(points[0], points[1]);
				drawer.drawIt();
			} else {
				throw new InvalidPointException();
			}

		} catch (InvalidPointException ipe) {

			System.err.println(ipe.getMessage());
		}
	}
	
	/**
	 * R x1 y1 x2 y2 Should create a new rectangle, whose upper left corner is
	 * (x1,y1) and lower right corner is (x2,y2). Horizontal and vertical lines
	 * will be drawn using the 'x' character.
	 * 
	 * @param canvas
	 * @param tokens
	 */
	public void doARectangle(Canvas canvas, String[] tokens) {

		try {
			Point[] points = getPoints(canvas, tokens, 2);

			Point.switchTwoPoints(points[0], points[1]);

			drawer.doARectangleOnTheCanvas(points[0], points[1]);
			drawer.drawIt();

		} catch (InvalidPointException ipe) {

			System.err.println(ipe.getMessage());
		}
	}
	
	/**
	 * Should fill the entire area connected to (x,y) with "colour" c. The
	 * behaviour of this is the same as that of the "bucket fill" tool in paint
	 * programs.
	 * 
	 * @param canvas
	 * @param tokens
	 */
	public void fillArea(Canvas canvas, String[] tokens) {
		try {
			Point[] points = getPoints(canvas, tokens, 1);

			drawer.fillAreaInTheCanvas(points[0], tokens[tokens.length - 1].charAt(0));
			drawer.drawIt();
		} catch (InvalidPointException ipe) {

			System.err.println(ipe.getMessage());
		}
	}
	
	/**
	 * Returns Instances of points base on the tokens from the keyboard input,
	 * 
	 * @param canvas
	 * 			  The canvas from where the points are going to be taken
	 * @param tokens
	 *            Tokens from the keyboard input
	 * @param numbeOfPoints
	 *            This is the number of points that the method is going to
	 *            return
	 * @return Array with the number of points on numbeOfPoints
	 * @throws InvalidPointException
	 */
	private Point[] getPoints(Canvas canvas, String[] tokens, Integer numbeOfPoints) throws InvalidPointException {

		Point[] points = new Point[numbeOfPoints];

		int j = 0;
		int pointPosition = 1;
		for (int i = 0; i < numbeOfPoints; i++) {
			Point point = new Point(Integer.valueOf(tokens[pointPosition]),
					Integer.valueOf(tokens[++pointPosition]));

			boolean isInsideCanvas = point.validatePoint(canvas);

			if (!isInsideCanvas) {
				throw new InvalidPointException();
			}

			points[j] = point;
			j++;
			pointPosition++;
		}
		return points;
	}
	
	/**
	 * Validates a correct keyboard input on the drawer menu
	 * 
	 * @param tokens
	 * @param numberOfTokens
	 * @return true or false, base on the input if it is correct or not
	 */
	public boolean validateDrawMenuInput(String[] tokens, int numberOfTokens) {
		boolean result = false;

		if (tokens.length == numberOfTokens) {
			result = true;
		}

		for (int i = 1; i < tokens.length; i++) {

			if ((i == (numberOfTokens - 1))
					&& (tokens[0].equals("B") || tokens[0].equals("b"))) {
				if (tokens[i].length() != 1) {
					result = false;
					break;
				}
				continue;
			}

			try {
				Integer.valueOf(tokens[i]);
			} catch (NumberFormatException nfe) {
				result = false;
				break;
			}
		}
		return result;
	}
	
	/**
	 * Validate that 2 points are in a vertical or horizontal line
	 * 
	 * @param from
	 * @param to
	 * @return true or false, base on the line that the 2 points create
	 */
	public boolean validateThereIsVerticalOrHorizontalLine(Point from, Point to) {
		boolean result = false;

		if ((from.getX() == to.getX()) || (from.getY() == to.getY())) {

			// Change position of the points if the first one is greater that the first
			Point.switchTwoPoints(from, to);

			result = true;
		}
		return result;
	}
}
