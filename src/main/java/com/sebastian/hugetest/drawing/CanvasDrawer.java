package com.sebastian.hugetest.drawing;

/**
 * Where the art comes reality, here the artist see his master piece on the
 * canvas.
 * 
 * @author Sebastian
 *
 */
public class CanvasDrawer {

	private Canvas canvas;

	public CanvasDrawer(Canvas canvas) {
		this.canvas = canvas;
	}
	
	/* Gettes && Setters */
	
	public Canvas getCanvas() {
		return canvas;
	}

	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}

	/**
	 * Show in the screen the current worn on the master piece of art.
	 */
	public void drawIt() {
		char[][] canvas = this.canvas.getCanvas();

		for (int j = 0; j <= this.canvas.getHeight() + 1; j++) {
			for (int i = 0; i <= this.canvas.getWidth() + 1; i++) {

				System.out.print(canvas[i][j]);
			}
			System.out.println();
		}
	}

	/**
	 * This method creates the margin on the canvas so the artist does no go out
	 * of bounds.
	 */
	public void doTheEdgesOnTheCanvas() {
		char[][] canvas = this.canvas.getCanvas();

		for (int i = 0; i <= this.canvas.getWidth() + 1; i++) {
			canvas[i][0] = '-';
			canvas[i][this.canvas.getHeight() + 1] = '-';
		}
		for (int i = 1; i <= this.canvas.getHeight(); i++) {
			canvas[0][i] = '|';
			canvas[this.canvas.getWidth() + 1][i] = '|';
		}

		this.canvas.setCanvas(canvas);
	}

	/**
	 * Creates a line on the canvas, vertical or horizontal line.
	 * 
	 * @param from
	 *            Point where the line is going to start
	 * @param to
	 *            Point where the line is going to end.
	 */
	public void doALineOnTheCanvas(Point from, Point to) {
		char[][] canvas = this.canvas.getCanvas();

		for (int i = from.getX(); i <= to.getX(); i++) {
			canvas[i][from.getY()] = 'X';
		}
		for (int i = from.getY(); i <= to.getY(); i++) {
			canvas[from.getX()][i] = 'X';
		}
		this.canvas.setCanvas(canvas);
	}

	/**
	 * Creates a rectangle on the master piece of art.
	 * 
	 * @param from
	 *            Point where the line is going to start
	 * @param to
	 *            Point where the line is going to end.
	 */
	public void doARectangleOnTheCanvas(Point from, Point to) {
		char[][] canvas = this.canvas.getCanvas();

		for (int i = from.getX(); i <= to.getX(); i++) {
			canvas[i][from.getY()] = 'X';
			canvas[i][to.getY()] = 'X';
		}
		for (int i = from.getY() + 1; i <= to.getY() - 1; i++) {
			canvas[from.getX()][i] = 'X';
			canvas[to.getX()][i] = 'X';
		}

		this.canvas.setCanvas(canvas);
	}

	/**
	 * This method fills and area with an specific color. The behavior of this
	 * is the same as that of the "bucket fill" tool in paint programs.
	 * 
	 * Base on Flood fill algorithm
	 * 
	 * @param point
	 *            From where it should start filing
	 * @param newColor
	 *            New color to fill the available area
	 */
	public void fillAreaInTheCanvas(Point point, Character newColor) {

		char oldColor = this.canvas.getCanvas()[point.getX()][point.getY()];

		// Checks trivial case where loc is of the fill color
		if (newColor == oldColor)
			return;

		fillLoop(this.canvas, point.getX(), point.getY(), newColor);
	}

	/**
	 * Recursively fills surrounding area looking in 4 directions
	 * right, left, up and down. 
	 * 
	 * @param canvas Where the art comes true
	 * @param x 
	 * @param y
	 * @param newColor
	 */
	private void fillLoop(Canvas canvas, int x, int y, char newColor) {

		// Fills to the left with the new color until it finds a character that can not be overwritten
		int fillLeft = x;
		do {
			canvas.getCanvas()[fillLeft][y] = newColor;
			fillLeft--;
		} while (fillLeft > 0 && canvas.getCanvas()[fillLeft][y] != 'X');
		fillLeft++;

		// Fills to the right with the new color until it finds a character that can not be overwritten
		int fillRight = x;
		do {
			canvas.getCanvas()[fillRight][y] = newColor;
			fillRight++;
		} while (fillRight < canvas.getWidth() + 1
				&& canvas.getCanvas()[fillRight][y] != 'X');
		fillRight--;

		// Checks if up or down are avaliable
		for (int i = fillLeft; i <= fillRight; i++) {
			if (y > 1 && canvas.getCanvas()[i][y - 1] != 'X'
					&& canvas.getCanvas()[i][y - 1] != newColor
					&& canvas.getCanvas()[i][y - 1] != '-') {
				fillLoop(canvas, i, y - 1, newColor);
			}
			if (y < canvas.getHeight() && canvas.getCanvas()[i][y + 1] != 'X'
					&& canvas.getCanvas()[i][y + 1] != newColor
					&& canvas.getCanvas()[i][y + 1] != '-') {
				fillLoop(canvas, i, y + 1, newColor);
			}
		}
	}
}
