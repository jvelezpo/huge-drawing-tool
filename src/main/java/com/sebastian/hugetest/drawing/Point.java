package com.sebastian.hugetest.drawing;

/**
 * Class that have information about points on the canvas
 * @author Sebastian
 *
 */
public class Point {
	
	private int x;
	private int y;
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/* Gettes && Setters */
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Validate that this point is inside the canvas where the user is drawing.
	 * 
	 * @param canvas It contains the area to paint.
	 * @return boolean
	 */
	public boolean validatePoint(Canvas canvas) {
		boolean result = false;
		
		if (this.x > 0 && this.y > 0 && this.x <= canvas.getWidth() && this.y <= canvas.getHeight()) {
			
			result = true;
		}
		
		return result;
	}
	
	/**
	 * Change position of the points if the first one is greater that the first
	 * @param a
	 * @param b
	 */
	public static void switchTwoPoints(Point a, Point b) {
		
		if ((a.getX() > b.getX()) || a.getY() > b.getY()) {
			
			Point temp = new Point(a.getX(), a.getY());
			
			a.setX(b.getX());
			a.setY(b.getY());
			
			b.setX(temp.getX());
			b.setY(temp.getY());
		}
	}
	
	/**
	 * Check if two points are the same by comparing the points x and y
	 */
	public boolean equals(Object obj) {
		
		if (!(obj instanceof Point))
            return false;
        if (obj == this)
            return true;
		
        Point point = (Point) obj;
        
		if (this.x == point.getX() && this.y == point.getY()) {
			return true;
		} else {
			return false;
		}
	}
}
