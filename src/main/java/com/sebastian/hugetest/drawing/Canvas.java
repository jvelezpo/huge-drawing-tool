package com.sebastian.hugetest.drawing;

/**
 * Canvas where art becomes true
 * 
 * @author Sebastian
 *
 */
public class Canvas {

	private int width;
	private int height;
	private char[][] canvas;

	public Canvas(int width, int height) {

		this.width = width;
		this.height = height;

		// Add a margin to the canvas so the artist wont go out of bounds
		this.canvas = new char[width + 2][height + 2];
	}

	/* Gettes && Setters */

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public char[][] getCanvas() {
		return canvas;
	}

	public void setCanvas(char[][] canvas) {
		this.canvas = canvas;
	}

}
