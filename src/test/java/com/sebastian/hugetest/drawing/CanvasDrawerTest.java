package com.sebastian.hugetest.drawing;

import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CanvasDrawerTest {

	@Mock
	Canvas canvas;
	
	private CanvasDrawer drawer;
	private char[][] theCanvas;
	private int x = 20, y = 4;
	
	@Before
	public void init() {
		drawer = new CanvasDrawer(canvas);
		theCanvas = new char[x + 2][y + 2];
	}
	
	@Test
	public void testDrawACanvas() {
		
	}
	
	@Test
	public void testDoTheEdgesOnTheCanvas() {
		
		when(this.canvas.getCanvas()).thenReturn(theCanvas);
		when(this.canvas.getWidth()).thenReturn(x);
		when(this.canvas.getHeight()).thenReturn(y);
		
		drawer.doTheEdgesOnTheCanvas();
		
		for (int i = 0; i <= x + 1; i++) {
			Assert.assertEquals('-', theCanvas[i][0]);
			Assert.assertEquals('-', theCanvas[i][y + 1]);
		}
		for (int i = 1; i <= this.canvas.getHeight(); i++) {
			Assert.assertEquals('|', theCanvas[0][i]);
			Assert.assertEquals('|', theCanvas[x + 1][i]);
		}
	}
	
	@Test
	public void testDoALineOnTheCanvas() {
		
		Point from = new Point(2, 2);
		Point to = new Point(10, 2);
		
		when(this.canvas.getCanvas()).thenReturn(theCanvas);
		
		drawer.doALineOnTheCanvas(from, to);
		
		for (int i = from.getX(); i <= to.getX(); i++) {
			Assert.assertEquals('X', theCanvas[i][from.getY()]);
		}
		for (int i = from.getY(); i <= to.getY(); i++) {
			Assert.assertEquals('X', theCanvas[from.getX()][i]);
		}
	}
	
	@Test
	public void testDoARectangleOnTheCanvas() {
		
		Point from = new Point(2, 2);
		Point to = new Point(4, 4);
		
		when(this.canvas.getCanvas()).thenReturn(theCanvas);
		
		drawer.doARectangleOnTheCanvas(from, to);
		
		for (int i = from.getX(); i <= to.getX(); i++) {
			Assert.assertEquals('X', theCanvas[i][from.getY()]);
			Assert.assertEquals('X', theCanvas[i][to.getY()]);
		}
		for (int i = from.getY() + 1; i <= to.getY() - 1; i++) {
			Assert.assertEquals('X', theCanvas[from.getX()][i]);
			Assert.assertEquals('X', theCanvas[to.getX()][i]);
		}
	}
}














