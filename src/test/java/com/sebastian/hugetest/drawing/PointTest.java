package com.sebastian.hugetest.drawing;

import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PointTest {
	
	@Mock
	Canvas canvas;
	
	@Test
	public void testCreateAPoint() {
		int x = 2;
		int y = 8;
		
		Point point = new Point(x, y);
		
		Assert.assertEquals(point.getX(), x);
		Assert.assertEquals(point.getY(), y);
	}
	
	@Test
	public void testvalidatePointInsideCanvas() {
		
		when(canvas.getWidth()).thenReturn(30);
		when(canvas.getHeight()).thenReturn(10);
		
		int x = 15;
		int y = 6;
		
		Point point = new Point(x, y);
		
		boolean result = point.validatePoint(canvas);
		
		Assert.assertTrue(result);
	}
	
	@Test
	public void testvalidatePointOutsideCanvas() {
		
		when(canvas.getWidth()).thenReturn(30);
		when(canvas.getHeight()).thenReturn(10);
		
		int x = 45;
		int y = 6;
		
		Point point = new Point(x, y);
		
		boolean result = point.validatePoint(canvas);
		
		Assert.assertFalse(result);
	}
	
	@Test
	public void testSwitchTwoPoints() {
		int x = 45;
		int y = 6;
		
		Point pointA = new Point(x, y);
		Point pointB = new Point(x - 6, y);
		
		Point.switchTwoPoints(pointA, pointB);
		
		Assert.assertEquals(pointA.getX(), x - 6);
		Assert.assertEquals(pointA.getY(), y);
		Assert.assertEquals(pointB.getX(), x);
		Assert.assertEquals(pointB.getY(), y);
	}
	
	@Test
	public void testSwitchTwoPointsResultWillBeSame() {
		int x = 45;
		int y = 6;
		
		Point pointA = new Point(x, y);
		Point pointB = new Point(x + 5, y);
		
		Point.switchTwoPoints(pointA, pointB);
		
		Assert.assertEquals(pointA.getX(), x);
		Assert.assertEquals(pointA.getY(), y);
		Assert.assertEquals(pointB.getX(), x + 5);
		Assert.assertEquals(pointB.getY(), y);
	}
}
