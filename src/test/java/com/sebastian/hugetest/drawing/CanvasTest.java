package com.sebastian.hugetest.drawing;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CanvasTest {

	@Test
	public void testCanvasConstruction() {
		int width = 6;
		int height = 9;
		
		Canvas canvas = new Canvas(width, height);
		
		Assert.assertEquals(canvas.getWidth(), width);
		Assert.assertEquals(canvas.getHeight(), height);
		Assert.assertEquals(canvas.getCanvas().length, width + 2);
		Assert.assertEquals(canvas.getCanvas()[0].length, height + 2);
	}
}
