package com.sebastian.hugetest.business;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.sebastian.hugetest.drawing.Canvas;
import com.sebastian.hugetest.drawing.CanvasDrawer;
import com.sebastian.hugetest.drawing.Point;
import com.sebastian.hugetest.exception.InvalidInputException;
import com.sebastian.hugetest.exception.InvalidPointException;
import com.sebastian.hugetest.presentation.InputAsker;

@RunWith(MockitoJUnitRunner.class)
public class CanvasProcessorTest {

	@Mock
	InputAsker asker;
	
	@Mock
	CanvasDrawer drawer;
	
	@Mock
	Canvas canvas;
	
	private Method getPoints;
	
	private CanvasProcessor processor;
	private int x = 20, y = 4;
	
	@Before
	public void init() throws NoSuchMethodException, SecurityException {
		processor = new CanvasProcessor(asker);
		processor.setDrawer(drawer);
		
		getPoints = CanvasProcessor.class.getDeclaredMethod("getPoints", Canvas.class, String[].class, Integer.class);
	}
	
	@Test
	public void testCreateNewCanvas() {
		
		when(asker.getInput()).thenReturn("20 4");
		
		processor.createNewCanvas();
		
		verify(drawer).doTheEdgesOnTheCanvas();
		verify(drawer).drawIt();
	}
	
	@Test
	public void testCreateNewCanvasWithWrongInput() throws InvalidInputException{
		
		when(asker.getInput()).thenReturn("I want to do art");
		
		processor.createNewCanvas();
	}
	
	@Test
	public void testDoALine() {
		String[] tokens = {"L", "1", "2", "6", "2"};
		Point from = new Point(1, 2);
		Point to = new Point(6, 2);
		
		when(canvas.getWidth()).thenReturn(x);
		when(canvas.getHeight()).thenReturn(y);
		
		processor.doALine(canvas, tokens);
		
		verify(drawer).doALineOnTheCanvas(from, to);
		verify(drawer).drawIt();
	}
	
	@Test
	public void testDoALineWithPointsNotVerticalOrHorizontal() throws InvalidPointException{
		String[] tokens = {"L", "1", "1", "4", "4"};
		
		when(canvas.getWidth()).thenReturn(x);
		when(canvas.getHeight()).thenReturn(y);
		
		processor.doALine(canvas, tokens);
	}
	
	@Test
	public void testDoARectangle() {
		String[] tokens = {"R", "3", "2", "6", "4"};
		Point from = new Point(3, 2);
		Point to = new Point(6, 4);
		
		when(canvas.getWidth()).thenReturn(x);
		when(canvas.getHeight()).thenReturn(y);
		
		processor.doARectangle(canvas, tokens);
		
		verify(drawer).doARectangleOnTheCanvas(from, to);
		verify(drawer).drawIt();
	}
	
	@Test
	public void testDoARectangleOutOfBounds() throws InvalidPointException{
		String[] tokens = {"R", "3", "2", "26", "4"};
		
		when(canvas.getWidth()).thenReturn(x);
		when(canvas.getHeight()).thenReturn(y);
		
		processor.doARectangle(canvas, tokens);
	}
	
	@Test
	public void testFillArea() throws InvalidPointException{
		String[] tokens = {"B", "3", "2", "o"};
		Point point = new Point(3, 2);
		
		when(canvas.getWidth()).thenReturn(x);
		when(canvas.getHeight()).thenReturn(y);
		
		processor.fillArea(canvas, tokens);
		
		verify(drawer).fillAreaInTheCanvas(point, 'o');
		verify(drawer).drawIt();
	}
	
	@Test
	public void testFillAreaWithAnInvalidPoint() throws InvalidPointException{
		String[] tokens = {"B", "30", "2", "o"};
		
		when(canvas.getWidth()).thenReturn(x);
		when(canvas.getHeight()).thenReturn(y);
		
		processor.fillArea(canvas, tokens);
	}
	
	@Test
	public void testGetPoints() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String[] tokens = {"B", "3", "2", "o"};
		
		when(canvas.getWidth()).thenReturn(x);
		when(canvas.getHeight()).thenReturn(y);
		
		getPoints.setAccessible(true);
		
		Point[] points = (Point[]) getPoints.invoke(processor, canvas, tokens, 1);
		
		Assert.assertEquals(points.length, 1);
		Assert.assertEquals(points[0].getX(), 3);
		Assert.assertEquals(points[0].getY(), 2);
	}
	
	@Test
	public void testValidateDrawMenuInput() {
		String[] tokens = {"B", "3", "2", "o"};
		
		boolean goodInput = processor.validateDrawMenuInput(tokens, 4);
		
		Assert.assertTrue(goodInput);
	}
	
	@Test
	public void testValidateDrawMenuInputWithWrongInput() throws NumberFormatException{
		String[] tokens = {"B", "Sebas", "Tian", "o"};
		
		processor.validateDrawMenuInput(tokens, 4);
	}
	
	@Test
	public void testValidateDrawMenuInputWithMoreTokens() {
		String[] tokens = {"B", "3", "o"};
		
		boolean goodInput = processor.validateDrawMenuInput(tokens, 4);
		
		Assert.assertFalse(goodInput);
	}
	
	@Test
	public void testValidateThereIsVerticalOrHorizontalLine() throws NumberFormatException{
		Point from = new Point(3, 2);
		Point to = new Point(6, 2);
		
		boolean good = processor.validateThereIsVerticalOrHorizontalLine(from, to);
		
		Assert.assertTrue(good);
	}
	
	@Test
	public void testValidateThereIsNotVerticalOrHorizontalLine() throws NumberFormatException{
		Point from = new Point(3, 2);
		Point to = new Point(6, 4);
		
		boolean good = processor.validateThereIsVerticalOrHorizontalLine(from, to);
		
		Assert.assertFalse(good);
	}
}

















