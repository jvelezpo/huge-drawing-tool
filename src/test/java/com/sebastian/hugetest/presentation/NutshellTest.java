package com.sebastian.hugetest.presentation;

import static org.mockito.Mockito.when;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.sebastian.hugetest.drawing.Canvas;
import com.sebastian.hugetest.exception.InvalidInputException;
import com.sebastian.hugetest.presentation.Nutshell;

@RunWith(MockitoJUnitRunner.class)
public class NutshellTest {

	@Mock
	List<Canvas> canvasList;
	@Mock
	InputAsker asker;
	
	@Rule
	public final ExpectedSystemExit exit = ExpectedSystemExit.none();
	
	private Nutshell nutshell;
	private Method createNewCanvas;
	
	@Before
	public void init() throws NoSuchMethodException, SecurityException {
		 nutshell = new Nutshell(asker);
		 createNewCanvas = Nutshell.class.getDeclaredMethod("createNewCanvas");
	}
	
	@Test 
	public void testCreateNewCanvas() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		when(asker.getInput()).thenReturn("20 4");

		createNewCanvas.setAccessible(true);

		Canvas canvas = (Canvas) createNewCanvas.invoke(nutshell);
		
		Assert.assertEquals(canvas.getWidth(), 20);
		Assert.assertEquals(canvas.getHeight(), 4);
	}
	
	@Test 
	public void testCreateNewCanvasWithTooManyArguments() throws InvalidInputException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		when(asker.getInput()).thenReturn("20 4 5");

		createNewCanvas.setAccessible(true);

		// This call will throw InvalidInputException
		createNewCanvas.invoke(nutshell);
	}

	@Test 
	public void testCreateNewCanvasLettersInsteadOfNumbers() throws InvalidInputException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		when(asker.getInput()).thenReturn("five six");

		createNewCanvas.setAccessible(true);

		// This call will throw InvalidInputException
		createNewCanvas.invoke(nutshell);
	}
	
	@Test
	public void testExitApplicationOnMenu() {
		
		when(asker.getInput()).thenReturn("3");
		
		//This is the line that gets the exception on Exit
		exit.expectSystemExitWithStatus(0);
		
		nutshell.inflatePrincipalMenu();
	}
}
