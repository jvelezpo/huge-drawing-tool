package com.sebastian.hugetest.presentation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.sebastian.hugetest.business.CanvasProcessor;
import com.sebastian.hugetest.drawing.Canvas;
import com.sebastian.hugetest.drawing.CanvasDrawer;

@RunWith(MockitoJUnitRunner.class)
public class CanvasNutshellTest {
	
	@Mock
	InputAsker asker;
	
	@Mock
	CanvasDrawer drawer;
	
	@Mock
	CanvasProcessor processor;

	private CanvasNutshell canvasNutshell;
	private Canvas canvas;
	private Method validateInput;
	private Method handleInput;
	
	@Before
	public void init() throws NoSuchMethodException, SecurityException {
		canvas = new Canvas(20, 4);
		canvasNutshell = new CanvasNutshell(canvas, asker);
		
		processor = new CanvasProcessor(asker);
		processor.setDrawer(drawer);
		
		canvasNutshell.setProcessor(processor);
		canvasNutshell.setDrawer(drawer);
		
		validateInput = CanvasNutshell.class.getDeclaredMethod("validateInput", String[].class, Integer.class);
		handleInput = CanvasNutshell.class.getDeclaredMethod("handleInput", String.class);
	}
	
	@Test
	public void testValidationOfCorrectInput() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		String[] tokens = {"L","1","1","14","1"};
		
		validateInput.setAccessible(true);
		
		boolean result = (Boolean) validateInput.invoke(canvasNutshell, tokens, 5);

		Assert.assertTrue(result);
	}
	
	@Test
	public void testGoBack() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		handleInput.setAccessible(true);
		
		boolean result = (Boolean) handleInput.invoke(canvasNutshell, "q");
		
		Assert.assertTrue(result);
	}
}


