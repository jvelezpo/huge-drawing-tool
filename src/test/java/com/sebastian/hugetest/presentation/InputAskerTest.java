package com.sebastian.hugetest.presentation;

import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.sebastian.hugetest.presentation.InputAsker;

@RunWith(MockitoJUnitRunner.class)
public class InputAskerTest {

	@Mock
	InputAsker asker;

	@Test
	public void askForAnyMessage() {
		when(asker.getInput()).thenReturn("Message");
		
		Assert.assertEquals(asker.getInput(), "Message");
	}
}
